#include <omp.h>
#include<iostream>
using namespace std;
int main(int argc, char ** argv )
{
  cout<<sizeof(int)<<endl;
  unsigned long sum = 0;
#pragma omp parallel firstprivate(sum)
  {
    #pragma omp for 
    for (unsigned int i=0; i<1000000; i++)
      sum += i;
    #pragma omp critical
    cout<<"Private sum is : "<<sum<<endl;
  }
  cout<<"Sum is : "<<sum<<endl;
}
