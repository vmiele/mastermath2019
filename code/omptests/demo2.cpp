#include <omp.h>
#include "utils.h"
#include<iostream>
using namespace std;
int main(int argc, char ** argv )
{
  unsigned int n = 1e7;
  vector<double> u = random(n);
  vector<double> v = random(n);
  vector<double> w(n);
#pragma omp parallel for
  for (unsigned int i=0; i<n; i++)
    w[i] = log(sqrt(u[i]+v[i])) + log(sqrt(u[i]-v[i]));
}
