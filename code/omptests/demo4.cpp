#include <omp.h>
#include<iostream>
using namespace std;
int main(int argc, char ** argv )
{
  unsigned long sum = 0, tsum = 0;
#pragma omp parallel firstprivate(tsum)
  {
#pragma omp for
    for (unsigned int i=0; i<1000000; i++)
      tsum += i;
#pragma omp critical
    {
      sum += tsum;
    }
  }
  cout<<"Sum is : "<<sum<<endl;
}
