#include <omp.h>
#include<iostream>
using namespace std;

void func()
{
  int i =0;
#pragma omp parallel firstprivate(i)
  {
#pragma omp for
    for (i=0; i<4; i++){
      cout<<i<<std::endl;
    }
  }
}

int main(int argc, char ** argv )
{
#pragma omp parallel
  {
    int i =0;
#pragma omp parallel firstprivate(i)
    {
#pragma omp for
      for (i=0; i<4; i++){
	cout<<i<<std::endl;
      }
    }
  }
  cout<<"##################"<<endl;
#pragma omp parallel
  {
    func();
  }
}
