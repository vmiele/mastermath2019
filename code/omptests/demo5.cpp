#include <omp.h>
#include "utils.h"
#include<iostream>
using namespace std;
int main(int argc, char ** argv )
{
  unsigned int n = 1e8;
  vector<double> u = random(n);
  vector<double> v = random(n);
  unsigned int nthr;
#pragma omp parallel
  {
#pragma omp single
    {
      nthr = omp_get_num_threads();
    }
  }
  vector<double> s(nthr, 0.); 
#pragma omp parallel
  {
    unsigned int thrn = omp_get_thread_num();
#pragma omp for schedule(static,1)
    for (unsigned int i=0; i<n; i++)
      s[thrn] += log(sqrt(u[i]+v[i])) + log(sqrt(u[i]-v[i]));
  }
}
