#include <omp.h>
#include<iostream>
using namespace std;
int main(int argc, char ** argv )
{
#pragma omp parallel
  {
#pragma omp critical
    {
      cout<<"Parallel or not?"<<endl;
    }
#ifdef _OPENMP
#pragma omp critical
    {
    cout<<"Hello I am thread "<<omp_get_thread_num()
	<<" over "<<omp_get_num_threads()<<" threads"<<endl;
    }
#endif
    int s = 0;
    //if (omp_get_thread_num()==0)
    for (unsigned int i=0; i<1e11; i++) s+= i; 
  }
}
