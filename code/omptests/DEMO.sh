# TOP TERMINAL
top -d 1

#DEMO1
g++ -O3 demo1.cpp -o demo1
./demo1
g++ -O3 -fopenmp demo1.cpp -o demo1
export OMP_NUM_THREADS=2
./demo1

#DEMO2
g++ -O3 demo2.cpp -o demo2
time ./demo2
g++ -O3 -fopenmp demo2.cpp -o demo2
export OMP_NUM_THREADS=1
time ./demo2
export OMP_NUM_THREADS=2
time ./demo2

#DEMO3
g++ -O3 demo3.cpp -o demo3
./demo3
g++ -O3 -fopenmp demo3.cpp -o demo3
export OMP_NUM_THREADS=4
./demo3

#DEMO4
g++ -O3 demo4.cpp -o demo4
./demo4
g++ -O3 -fopenmp demo4.cpp -o demo4
export OMP_NUM_THREADS=4
./demo4
