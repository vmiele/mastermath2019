#include<omp.h>
#include<iostream>
#include<unistd.h>
using namespace std;
int main(int argc, char ** argv )
{
#define CLASSIC
#ifdef CLASSIC
  
  bool abort = false;
#pragma omp parallel for
 for (unsigned int i=0; i<20; i++){
   if (i==0) {
     abort = true;
#pragma omp flush (abort)
   }
   if(!abort){
     std::cout<<i<<" treated"<<std::endl;
     sleep(1);
   }
 }
 
#else

 bool abort = false;
#pragma omp parallel
 {
   unsigned int k = omp_get_thread_num();
   for (unsigned int i=(k*5); i<((k+1)*5); i++){
     if (i==0) {
       abort = true;
#pragma omp flush (abort)
     }
     if(!abort){
       std::cout<<i<<" treated"<<std::endl;
       sleep(1);
     }
   } 
 }
 
#endif
}
