#
# A test of `multiprocessing.Pool` class
#
# Copyright (c) 2006-2008, R Oudkerk
# All rights reserved.
#

import multiprocessing
import time
import random
import sys

#
# Functions used by test code
#

def calculate(func, args):
    result = func(*args)
    return '%s says that %s%s = %s' % (
        multiprocessing.current_process().name,
        func.__name__, args, result
        )

def calculatestar(args):
    return calculate(*args)

def bidon(a):
    i=0
    while(i<(a*10000000)):
        i+=1
    return a


def test():
    print 'cpu_count() = %d\n' % multiprocessing.cpu_count()

    #
    # Create pool
    #

    PROCESSES = 2
    print 'Creating pool with %d processes\n' % PROCESSES
    pool = multiprocessing.Pool(PROCESSES)
    print 'pool = %s' % pool
    print


    TASKS = [(bidon, (2,)), (bidon, (7,)), (bidon, (20,)), (bidon, (9,)), (bidon, (1,)), (bidon, (5,))]

    imap_unordered_it = pool.imap_unordered(calculatestar, TASKS)

    t = time.time()
    print 'Unordered results using pool.imap_unordered():'
    for x in imap_unordered_it:
        print '\t', x
    print time.time() - t

   
if __name__ == '__main__':
    test()
