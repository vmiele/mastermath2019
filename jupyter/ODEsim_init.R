library("deSolve")
rm(list=ls())

#################################
######## FOR SONIA  #############
######## WAS COPIED #############
#################################
INTPOS=0.3
INTNEG=0.05
X0=0.2227
A0=5

## NB: tu peux aussi faire des boucles comme
## for(INTPOS in c(0.1,0.5,1)){ ... }


biomass=c()
nbspecies=c()
#################################

#################################
######## FOR SONIA  #############
######## WAS COPIED #############
#################################
PFILES=c("positive_proba.net", # chilean web
    paste("Bank/positive_proba_sim",1:500,".net",sep="")) # 500 random networks
NFILES=c("negative_proba.net", # chilean web
    paste("Bank/negative_proba_sim",1:500,".net",sep="")) # 500 random networks

numfile=1
PFILE=PFILES[numfile]
NFILE=NFILES[numfile]
#################################

# General things to define
#------------------------------------------------------------------
## Loads the trophic network
n = max(unlist(read.table("trophic_proba.net"))) #number of species
TR = matrix(0,n,n)
tr.net = read.table("trophic_proba.net")
apply(tr.net,1,FUN=function(v){TR[v[1],v[2]] <<- v[3]; NULL})
# On remplace cluster 4 par le plancton seulement:
TR[,4] <- 0
TR[4,] <- 0
TR[6,4] <- 2/5 # prop of species in cluster which consume plankton
TR[10,4] <- 2/5
TR[5,4] <- 2/2
TR[13,4] <- 3/4

## Sets initial biomasses
State = rep(1,n)
#State <- runif(n,min=0.9,max=1.1)
State0 <- State



#################################
######## FOR SONIA  #############
######## WAS COPIED #############
#################################
NTIneg = TRUE
intneg <- INTNEG
NTIpos = TRUE
intpos <- INTPOS
RANDOM = FALSE
#################################

## Choose the source of parameter values used
rev1 = FALSE # version used for rev 1 of PLOS Biol

#------------------------------------------------------------------
# Loads matrices of interactions - from data or generated outside this code
#------------------------------------------------------------------
mobile = read.table("mobile.txt")$V1
mobile[4] <- "S"
if(TRUE){
  m <- read.table("masses.txt")$V1 # garder les algues au-dessus de 1?
  # mettre les algues à au moins 1
  mt <- 1
  m[3] <- mt
  m[6] <- mt # keep? Algae + barnacles
  m[10] <- mt # keep? Algae + barnacles
  m[11] <- mt
  m[12] <- mt
  #m[9] <- 7#m[9]*15
}
basal = sapply(1:n, function(i) sum(TR[i,])==0)

# Loads the facilitation network (decrease in mortality)
POS = matrix(0,n,n)
FAC <- matrix(data = rep(0,n*n),nrow = n)
REC <- matrix(data = rep(0,n*n),nrow = n)
REF <- matrix(data = rep(0,n*n),nrow = n)

if(NTIpos == TRUE){
#################################
######## FOR SONIA  #############
######## WAS COPIED #############
#################################
  p.net = try(read.table(PFILE))
#################################
  apply(p.net,1,FUN=function(v){POS[v[1],v[2]] <<- v[3]; NULL})
  if(RANDOM == TRUE) POS = matrix(sample(POS),n,n)
  for (i in 1:n){
    for(j in 1:n){
      if(POS[i,j]>0){
        if(basal[j]){     
          REC[i,j]=POS[i,j]   # if target sp is a plant: increase in recruitment, r
        }else if(mobile[i]=="S" & mobile[j]=="M"){
          REF[i,j] = POS[i,j] # sessile sp protect mobile sp from their consumers      
        }else{
          FAC[i,j] = POS[i,j]   # in all the other cases, pos effect on mortality (increased survival)
        }
      }
    }
  }
}

# Loads the competition network (increase in mortality; competition  )
NEG = matrix(0,n,n)       
MOR <- matrix(data = rep(0,n*n),nrow = n)
COMP <- matrix(data = rep(0,n*n),nrow = n)
INT <- matrix(data = rep(0,n*n),nrow = n)

if(NTIneg == TRUE){
#################################
######## FOR SONIA  #############
######## WAS COPIED #############
#################################
  n.net = try(read.table(NFILE))
#################################
  apply(n.net,1,FUN=function(v){NEG[v[1],v[2]] <<- v[3]; NULL})
  if(RANDOM == TRUE) NEG = matrix(sample(NEG),n,n)
  for (i in 1:n){
    for(j in 1:n){
      if (NEG[i,j]>0){
        if(mobile[i]=="S" & mobile[j]=="S"){ 
        #if(mobile[j]=="S" & basal[j]){ 
          COMP[i,j] = NEG[i,j] # target is a sessile species - comp for space
        }else if(mobile[i]=="M" & mobile[j]=="M"){
          INT[i,j] = NEG[i,j]
        }else{
          MOR[i,j] = NEG[i,j] # target is a non-basal sessile species - effect on survival
        }
      }
    }
  }
}

#------------------------------------------------------------------
# Trophic parameters
#------------------------------------------------------------------

# Réponse fonctionnelle
q <- 1 # q=0: type II, q=1: type III

# Parameters for basal species
TL = rep(0,n) # initialise trophic level - 1 for plants
for(i in 1:n){
  TL[i] <- ifelse(sum(TR[i,])==0,1,0)
}

# Define a vector of trophic level for each species: 1 for basal species, and 1 + mean(resources) for other species
for(rep in 1:10){
  for(i in 1:n){
    nb.prey = sum(TR[i,])
    if(nb.prey>0){
      TL.temp = 0
      for(k in 1:n){
        TL.temp = TL.temp + TR[i,k]*TL[k]
      }
      TL[i] <- 1 + TL.temp/nb.prey
    }
  }
}

# Body masses: consumers are about 3.2 times larger than their prey
# we fix biomasses such that all sp of a given TL have the same size
# ratio of sizes between pred and prey are based on diff in real biomasses (largest sp has a biomass of 10)
if(FALSE){
  m <- rep(0,n)
  for(i in 1:n){
    #m[i]=3.2^(floor(TL[i]+0.5)-1)
    m[i]=3.2^(round(TL[i])-1)
  }
  m[6] <- 1
  m[10] <- 1
}

r = rep(0,n)
for(i in 1:n){
  if(rev1 == TRUE){
    r[i] <- ifelse(sum(TR[i,])>0,0,m[i]^(-0.25))
  }else{
    r[i] <- ifelse(sum(TR[i,])>0,0,m[i]^(-0.25))
    #r[i] <- ifelse(sum(TR[i,])>0,0,1)
  }
}

K <- rep(1,n) # Half stauration density for plant growth - Brose 2006: 1, Brose 2008 [0.1 - 0.2]
x = rep(0,n) # metabolism
h = rep(0,n) # handling time
a <- matrix(data = rep(0,n*n),nrow = n) # attack rate
e <- rep(0,n) # biomass conversion efficiency

for(i in 1:n){
  if(rev1 == TRUE){
    e[i] <- ifelse(r[i]>0,0.45,0.85)
  }else{
    e[i] <- ifelse(r[i]>0,0.45,0.85)
    #e[i] <- 0.85#ifelse(TL[i]>2,0.85,0.45) # 0.85 for carnivores, 0.45 for herbivores
  }
}

# Constants for consumers
if(rev1 == TRUE){
  y <- 8 # maximum ingestion rate of consumer per unit metabolic rate of resource/prey
#################################
######## FOR SONIA  #############
######## WAS COPIED #############
#################################
  x0 <- X0
  a0 <- A0
#################################
  eps <- 0.01064
  #Bo=0.5
  for(i in 1:n){
    x[i] <- ifelse(r[i]>0,0.138,x0*m[i]^(-0.25))
    h[i] <- ifelse(x[i]>0,1/(y*x[i]),0) 
    for(j in 1:n){
      a[i,j] = TR[i,j]*a0*x[i]*exp(-eps*m[i]/m[j])
      #a[i,j] = TR[i,j]*x[i]*y/Bo
    }
  }
  
}else{
#################################
######## FOR SONIA  #############
######## WAS COPIED #############
#################################
  x0 <- X0
  a0 <- A0
#################################
  h0 = 0.4 # from Rall 2011
  eps <- 0.002 # from Rall 2011, Kalinkat 2013
  for(i in 1:n){
    x[i] <- ifelse(r[i]>0,0.138,x0*m[i]^(-0.25))
    h[i] <- ifelse(r[i]>0,0,h0*m[i]^(-0.75)) # expression from Kalinkat 2013, exponent as well
    for(j in 1:n){
      a[i,j] = TR[i,j]*a0*m[j]^(0.5)*(m[i]/m[j])*exp(-eps*m[i]/m[j]) # shape and exp from Kalinkat 2013
      #a[i,j] = TR[i,j]*a0*m[i]^(0.6)*m[j]^(0.3)
    }
  }

}

# Predators' preference
w = rep(0,n)
for(i in 1:n){
  w[i] <- ifelse(sum(TR[i,])>0,1/sum(TR[i,]),0)
}

#------------------------------------------------------------------
# Non-trophic parameters
#------------------------------------------------------------------
# Effects on metabolism
c <- intneg # Intensity of competition for space
d <- intneg # Intensity of predator interference
xneg <- 1 + intneg # Intensity of the neg effect on metabolism
xpos <- 1 + intpos  # Intensity of the pos effect on metabolism
rpos <- 1 + intpos  # pos effect on basal sp recruitment
bpos <- 1+intpos # protection effect from consumers
xmax <- xneg*x 
xmin <- x/xpos 
rmax <- rpos*r
#bmin <- matrix(0,n,n)
bmin <- a/bpos


maxtime <- 100
steplenth <- 1
tsteps <- maxtime/steplenth+1
analyzetime <- maxtime*0.8
Time <-seq(0, maxtime, length = tsteps)

## Running
Pars <-c(r=r, K=K, x=x, e=e, h=h, a=a, xmin, xmax, c, d,rmax)

source("func_foodwebmod_rev2_rcpp.R")

