#------------------------------------------------------------------
foodwebmod <-function(Time, State, Pars){
    
    ## Very small biomass values are put to zero
    for (i in 1:length(State)){ # species i of interest
        State[i] <- ifelse(State[i] < 10^(-6), 0, State[i])
        State[i] <- ifelse(State[i] < 0, 0, State[i])
    } 
    
    ## Definition of the equations
    dState = rep(0, length(State))
    
    for (i in 1:length(State)){ # species i of interest
        g <- 1
        mor.temp <- 0
        fac.temp <- 0
        rec.temp <- 0
        b.temp <- 0
        xnew <-0
        fal.out.temp <- 0 # eats
        fal.in.temp <- 0 # is eaten
        
        for (j in (1:length(State))){
                                        # Comp for space
            g <- g - COMP[j,i]*c*State[j]
            
                                        # NT effects on metabolism (neg effect from sessile to mobile sp and pos effects from sessile to sessile and from mobile to anything)
            mor.temp <- mor.temp + MOR[j,i]*State[j]
            fac.temp <- fac.temp + FAC[j,i]*State[j]
            
                                        # NT effect on recruitment
            rec.temp <- rec.temp + REC[j,i]*State[j] 
        }
                                        # Calculate elements for the NTI
        for (j in (1:length(State))){
            
                                        # Functional response - eat
            for (k in (1:length(State))){   
                b.temp <- b.temp + REF[k,j]*State[k]
            }
            
            int.out.temp <- 0  # Interference with predator i from all the predators k of prey j     
            prey.out.temp <- 0 
            for (k in (1:length(State))){   
                int.out.temp <- int.out.temp + TR[k,j]*d*INT[k,i]*State[k]
                prey.out.temp <- prey.out.temp + (a[i,k] + bmin[i,k]*b.temp)/(1+b.temp)*TR[i,k]*State[k]^(1+q) # take out of (before) the j loop
            }
            fal.out.temp <- fal.out.temp+e[i]*w[i]*TR[i,j]*(a[i,j] + bmin[i,j]*b.temp)/(1+b.temp)*State[j]^(1+q)/(1+int.out.temp+w[i]*h[i]*prey.out.temp)
            
                                        # Functional response - is eaten
            for (k in (1:length(State))){   # peut être sorti de la boucle j
                b.temp <- b.temp + REF[k,i]*State[k]
            }
            int.in.temp <- 0      
            prey.in.temp <- 0 
            for (k in (1:length(State))){   
                int.in.temp <- int.in.temp + TR[k,i]*d*INT[k,j]*State[k]
                prey.in.temp <- prey.in.temp + (a[j,k] + bmin[j,k]*b.temp)/(1+b.temp)*TR[j,k]*State[k]^(1+q)
            }
            fal.in.temp <- fal.in.temp+w[j]*TR[j,i]*(a[j,i] + bmin[j,i]*b.temp)/(1+b.temp)*State[i]^(1+q)/(1+int.in.temp+w[j]*h[j]*prey.in.temp)*State[j]
        }# end for j
        
        if(g<0){
            g = 0
        }
        
        rnew = (r[i] + rec.temp*rmax[i])/(1+rec.temp)
                                        #xnew = (x[i] + mor.temp*xmax[i] + fac.temp*xmin[i])/(1+mor.temp + fac.temp)
        xnew = x[i] - mor.temp*(xmax[i]-x[i])/(1+mor.temp) + fac.temp*(x[i]-xmin[i])/(1+fac.temp)
                                        #cat(xnew,x[i],"\n")
        dState[i] = (rnew*g*(1 - State[i]/K[i]) + fal.out.temp*g)*State[i] - xnew*State[i] - fal.in.temp 
    } # end for i
    return<-list(dState)
    
} # end function foodwebmod

library(Rcpp)
library(inline)

cppFunction('NumericVector foodwebmodC_core(NumericVector State,
NumericMatrix TR, NumericMatrix COMP,  NumericMatrix INT, NumericMatrix MOR,
NumericMatrix FAC, NumericMatrix REC, NumericMatrix REF,
NumericMatrix bmin, NumericMatrix a, NumericVector e, NumericVector w, NumericVector h,
NumericVector K, NumericVector r, NumericVector rmax, NumericVector x, NumericVector xmin, NumericVector xmax,
double c, double d, int q){

NumericVector dState(State.size());
for (int i=0; i<State.size(); i++){ // species i of interest
 double g = 1;
 double mortemp = 0;
 double factemp = 0;
 double rectemp = 0;
 double btemp = 0;
 double xnew =0;
 double falouttemp = 0; // eats
 double falintemp = 0; // is eaten
 
 for (int j=0; j<State.size(); j++){
  // Comp for space
  g = g - COMP(j,i)*c*State[j];
 
  // NT effects on metabolism (neg effect from sessile to mobile sp and pos effects from sessile to sessile and from mobile to anything)
  mortemp = mortemp + MOR(j,i)*State[j];
  factemp = factemp + FAC(j,i)*State[j];
 
  // NT effect on recruitment
  rectemp = rectemp + REC(j,i)*State[j];
 }
 // Calculate elements for the NTI
 for (int j=0; j<State.size(); j++){
 
  // Functional response - eat
  for (int k=0; k<State.size(); k++){ 
   btemp = btemp + REF(k,j)*State[k];
  }
 
  double intouttemp = 0; // Interference with predator i from all the predators k of prey j 
  double preyouttemp = 0;

  for (int k=0; k<State.size(); k++){ 
   intouttemp = intouttemp + TR(k,j)*d*INT(k,i)*State[k];
   preyouttemp = preyouttemp + (a(i,k) + bmin(i,k)*btemp)/(1+btemp)*TR(i,k)*pow(State[k],(1+q)); // take out of (before) the j loop
  }
  falouttemp = falouttemp+e[i]*w[i]*TR(i,j)*(a(i,j) + bmin(i,j)*btemp)/(1+btemp)*pow(State[j],(1+q))/(1+intouttemp+w[i]*h[i]*preyouttemp);
 
  // Functional response - is eaten
  for (int k=0; k<State.size(); k++){ // peut être sorti de la boucle j
   btemp = btemp + REF(k,i)*State[k];
  }
  double intintemp = 0;
  double preyintemp = 0;

  for (int k=0; k<State.size(); k++){ 
   intintemp = intintemp + TR(k,i)*d*INT(k,j)*State[k];
   preyintemp = preyintemp + (a(j,k) + bmin(j,k)*btemp)/(1+btemp)*TR(j,k)*pow(State[k],(1+q));
  }
  falintemp = falintemp+w[j]*TR(j,i)*(a(j,i) + bmin(j,i)*btemp)/(1+btemp)*pow(State[i],(1+q))/(1+intintemp+w[j]*h[j]*preyintemp)*State[j];
 }

 if(g<0){
  g = 0;
 }
 double rnew = (r[i] + rectemp*rmax[i])/(1+rectemp);
 xnew = x[i] - mortemp*(xmax[i]-x[i])/(1+mortemp) + factemp*(x[i]-xmin[i])/(1+factemp);
 dState[i] = (rnew*g*(1 - State[i]/K[i]) + g*falouttemp)*State[i] - xnew*State[i] - falintemp;
}
return dState;
}
',includes=c("#include<cmath>","#include<iostream>"))


foodwebmodC <-function(Time, State, Pars){    
    ## Very small biomass values are put to zero
    for (i in 1:length(State)){ # species i of interest
        State[i] <- ifelse(State[i] < 10^(-6), 0, State[i])
        State[i] <- ifelse(State[i] < 0, 0, State[i])
    }
    return<-list(foodwebmodC_core(State, TR, COMP, INT, MOR, FAC, REC, REF, bmin, a, e, w, h, K, r, rmax, x, xmin, xmax, c, d, q))
}
