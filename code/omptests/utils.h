#include<vector>
#include<algorithm>
#include<cstdlib>
#include<cmath>
struct gen_rand { 
public:
    gen_rand() {}
    double operator()() { 
      return (1.0*rand()) / RAND_MAX;
    }
};

std::vector<double> random(int n){
  std::vector<double> x;
  x.reserve(n);
  generate_n(std::back_inserter(x), n, gen_rand());
  return(x);
}
